import { BankingDAO } from "../src/daos/banking-dao";
import { BankingDaoPostgres } from "../src/daos/banking-dao-postgres";
import { Client } from "../src/Entities";
import { Account } from "../src/Entities";
import { connection } from "../src/connection";

const bankDAO:BankingDAO = new BankingDaoPostgres();

// 1
test("Create a Client", async()=>{
    const testClient:Client = new Client(0,"John","Dow");
    const result:Client = await bankDAO.createClient(testClient);
    expect(result.client_id).not.toBe(0);
})

// 2
test("Get all Clients", async()=>{

    let client1:Client = new Client(0,"Michael", "Jones");
    let client2:Client = new Client(0, "Cruz", "Danny");
    let client3:Client = new Client(0, "Lionel", "Messi");

    await bankDAO.createClient(client1)
    await bankDAO.createClient(client2)
    await bankDAO.createClient(client3)

    const clients:Client[] = await bankDAO.getAllClients();

    expect(clients.length).toBeGreaterThanOrEqual(3);
})

//3
test("Get Client by ID", async()=>{
    let client:Client = new Client(0,"Cristiano", "Ronaldo");
    client = await bankDAO.createClient(client);

    let retrievedClient:Client = await bankDAO.getClientByID(client.client_id);

    expect(retrievedClient.first_name).toBe(client.first_name);

})

//4
test("Update Client", async()=>{
    let client:Client = new Client(0,"Jaime", "Moreno");
    client = await bankDAO.createClient(client);

    client.first_name = "Martins";

    client = await bankDAO.updateClient(client);

    expect(client.first_name).toBe("Martins")
})

//5
test("Delete Client", async()=>{
    let client:Client = new Client(0,"Jack", "Grealish");
    client = await bankDAO.createClient(client);

    const result:boolean = await bankDAO.deleteClientByID(client.client_id);

    expect(result).toBeTruthy();
})


// // ///////////////// ACCOUNT

//6
test("Create an Account", async()=>{

    const testClient:Client = new Client(0,"Justin","Quiles");
    const cResult:Client = await bankDAO.createClient(testClient);

    const testAccount:Account = new Account(0,cResult.client_id,0);
    const aResult:Account = await bankDAO.createAccount(testAccount);

    expect(aResult.account_id).not.toBe(0);
})

//7
test("GetAccounts by Client ID", async()=>{

    const testClient:Client = new Client(0,"Lenny","Taverez");
    const cResult:Client = await bankDAO.createClient(testClient);

    let account1:Account = new Account(0,cResult.client_id, 0);
    let account2:Account = new Account(0,cResult.client_id, 0);
    let account3:Account = new Account(0,cResult.client_id, 0);

    await bankDAO.createAccount(account1)
    await bankDAO.createAccount(account2)
    await bankDAO.createAccount(account3)

    const accounts:Account[] = await bankDAO.getAccountByID(cResult.client_id);

    expect(accounts.length).toEqual(3)

})

//8
test("Get Account", async()=>{

    // create client
    const testClient:Client = new Client(0,"Romelu","Lukaku");
    const cResult:Client = await bankDAO.createClient(testClient);

    // create Account
    let account1:Account = new Account(0,cResult.client_id, 0);
    let retrievedAccount:Account = await bankDAO.createAccount(account1);

    let tempAccount:Account = await bankDAO.getAccount(retrievedAccount.account_id);

    expect(tempAccount.account_id).not.toBe(0);
})

//9
test("Update Account", async()=>{

    // create client
    const testClient:Client = new Client(0,"Mikel","Arteta");
    const cResult:Client = await bankDAO.createClient(testClient);

    // create Account
    let account1:Account = new Account(0,cResult.client_id, 0);
    let retrievedAccount:Account = await bankDAO.createAccount(account1);

    let account2:Account = new Account(retrievedAccount.account_id,5,500); // new client id and new value

    let tempAccount:Account = await bankDAO.updateAccount(account2);

    expect(tempAccount.account_id).toEqual(account2.account_id);
    expect(tempAccount.c_id).toEqual(account2.c_id);
    expect(tempAccount.balance).toEqual(account2.balance);
})
//10
test("Deposit into Account", async()=>{
        // create client
        const testClient:Client = new Client(0,"Benito","Santos");
        const cResult:Client = await bankDAO.createClient(testClient);
    
        // create Account with balance 0
        let account1:Account = new Account(0,cResult.client_id, 0);
        let retrievedAccount:Account = await bankDAO.createAccount(account1);

        let account2:Account = new Account(retrievedAccount.account_id,cResult.client_id, 500);
        let retrivedAccount2:Account = await bankDAO.updateAccount(account2);

        expect(retrivedAccount2.account_id).toEqual(retrievedAccount.account_id)
        expect(retrivedAccount2.balance).toEqual(500);
})

// 11
test("Delete Account", async()=>{

    const testClient:Client = new Client(0,"Jowell","Randy");
    const cResult:Client = await bankDAO.createClient(testClient);

    // create Account with balance 0
    let account1:Account = new Account(0,cResult.client_id, 0);
    let retrievedAccount:Account = await bankDAO.createAccount(account1);

    const result:boolean = await bankDAO.deleteAccountByID(retrievedAccount.account_id);

    expect(result).toBeTruthy();


})

afterAll(async()=>{
    connection.end();
})