import { connection } from "../src/connection";

test("Should create a connection", async()=>{
    const result = await connection.query('select * from client');
    console.log(result);
})

afterAll(()=>{
    connection.end();
})