import express from 'express';
import BankingService from './services/banking-services';
import { BankingServiceImpl } from './services/banking-services-impl';
import { Client } from './Entities';
import { Account } from './Entities';
import { MissingResourceError } from './errors';
import { InsufficientFundsError } from './errors';


const app = express();

app.use(express.json());

const bankingService:BankingService = new BankingServiceImpl();

app.post("/clients",async (req,res)=>{
    let client:Client = req.body;
    client = await bankingService.registerClient(client);
    res.status(201);
    res.send(client);
})

app.get("/clients",async(req,res)=>
{
    try 
    {
        const clients: Client[] = await bankingService.retrieveAllClients();
        res.status(200);
        res.send(clients);    
    } catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }    
    }
})

app.get("/clients/:id", async(req,res)=>
{
    try 
    {
        const clientID = Number(req.params.id);
        const client:Client = await bankingService.retrieveClientByID(clientID);
        res.send(client);

    } catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }    
    }
})

app.put("/clients/:id", async(req,res)=>{ // may have to add id in the body also 
    // client with id 
    try 
    {
        let clientID = Number(req.params.id);
        let client_body = req.body;
        let client:Client = await bankingService.retrieveClientByID(clientID); // get client with ID requested
        client.first_name = client_body.first_name; // update first name
        client.last_name = client_body.last_name;// update last name
        client = await bankingService.modifyClient(client);
        res.send(client);
    } 
    catch (error) 
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }    
    }
})


app.delete("/clients/:id", async(req,res)=>{
    try 
    {
        const clientID = Number(req.params.id);
        const client:boolean = await bankingService.removeClientByID(clientID);
        res.status(205);
        res.send(client); // returns true for deleted
    } 
    catch (error)
    {
         res.status(404);
         res.send(error)
     }
})

app.post("/clients/:id/accounts" , async(req,res)=>{

    try 
    {
        const body:Account = req.body; // assuming passing in correct id
        const client:Client = await bankingService.retrieveClientByID(body.c_id)// if client doesnt exist then this will return error
        const account:Account = await bankingService.registerAccount(body);
        res.status(201);
        res.send(account);

    } 
    catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }    
        
    }
})

// get all accounts for client id(5)
app.get("/clients/:id/accounts", async(req,res)=>{

    try 
    {
        const clientID = Number(req.params.id);
        const result:Account[] = await bankingService.getAccountsByID(clientID);
        res.send(result); // returns true for deleted
    }
    catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
})

app.get("/accounts/:id", async(req,res)=>{
    try 
    {
        const accountID = Number(req.params.id);
        const result:Account = await bankingService.getAccounts(accountID);
        res.send(result); // returns true for deleted
    } 
    catch (error)
    {
         res.status(404);
         res.send(error)
     }
})

app.put("/accounts/:id", async(req,res)=>{
    try
    {
        const accountID = Number(req.params.id);
        const tempAccount:Account = await bankingService.getAccounts(accountID)

        const clientID = req.body.c_id;
        const balance = req.body.balance;
    
        const account:Account = new Account(tempAccount.account_id, clientID, balance);
        const result:Account = await bankingService.updateAccount(account);    

        res.send(result);
    } catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
})

app.delete("/accounts/:id", async(req,res)=>{
    try 
    {
        const accountID = Number(req.params.id);
        const account:boolean = await bankingService.removeAccountByID(accountID);
       //res.status(205);
        res.send(account); // returns true for deleted
    } 
    catch (error)
    {
         res.status(404);
         res.send(error)
     }
})

app.patch("/accounts/:id/deposit", async(req,res)=>{
    
    try 
    {   
        const tempAccount:Account = await bankingService.getAccounts(Number(req.params.id));//test
        const AddingBalance = req.body.amount;
        const NewBalance = AddingBalance + tempAccount.balance;
        const account:Account = new Account(tempAccount.account_id, tempAccount.c_id,NewBalance); 
        const result:Account = await bankingService.depositOrWithdrawIntoAccount(account);
        res.send(result);
    } catch (error)
    {
        res.status(404);
        res.send(error)
    }
})

app.patch("/accounts/:id/withdraw", async(req,res)=>{
    
    try 
    {   
        const tempAccount:Account = await bankingService.getAccounts(Number(req.params.id));//test
        
        const WithdrawAmount = req.body.amount;

        if(WithdrawAmount > tempAccount.balance)
        {
            res.status(422);
            throw new InsufficientFundsError(`Insufficient Funds`);
        }
        const NewBalance = tempAccount.balance - WithdrawAmount;

        const account:Account = new Account(tempAccount.account_id, tempAccount.c_id,NewBalance); 
        const result:Account = await bankingService.depositOrWithdrawIntoAccount(account);
        res.send(result);
    } catch (error)
    {
        res.status(404);
        res.send(error)
    }
})

app.listen(3000,()=>{console.log("Application started")});