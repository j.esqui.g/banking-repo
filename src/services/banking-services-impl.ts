import { Client } from "../Entities";
import { Account } from "../Entities";
import BankingService from "./banking-services";
import { BankingDaoPostgres } from "../daos/banking-dao-postgres";
import { BankingDAO } from "../daos/banking-dao";

export class BankingServiceImpl implements BankingService
{
    bankingDAO:BankingDAO = new BankingDaoPostgres();

    registerClient(client: Client): Promise<Client> 
    {
        return this.bankingDAO.createClient(client);
    }
    retrieveAllClients(): Promise<Client[]> 
    {
        return this.bankingDAO.getAllClients();
    }
    retrieveClientByID(client_id: number): Promise<Client> 
    {
        return this.bankingDAO.getClientByID(client_id);
    }
    modifyClient(client:Client): Promise<Client>
    {
        return this.bankingDAO.updateClient(client);
    }

    removeClientByID(client_id: number): Promise<boolean>
    {
        return this.bankingDAO.deleteClientByID(client_id);
    }

    async registerAccount(account:Account): Promise<Account>
    {
        return this.bankingDAO.createAccount(account);
    }
    
    getAccountsByID(c_id: number): Promise<Account[]>
    {
        return this.bankingDAO.getAccountByID(c_id);
    }

    getAccounts(account_id: number): Promise<Account>
    {
        return this.bankingDAO.getAccount(account_id);
    }
    removeAccountByID(account_id:number):Promise<boolean>
    {
        return this.bankingDAO.deleteAccountByID(account_id);
    }

    depositOrWithdrawIntoAccount(account:Account): Promise<Account>
    {
        return this.bankingDAO.updateAccountDepositOrWithdraw(account);
    
    }
    updateAccount(account: Account): Promise<Account> 
    {
        return this.bankingDAO.updateAccount(account)
    }
}