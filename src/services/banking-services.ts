import { Client } from "../Entities";
import { Account } from "../Entities";

export default interface BankingService{

    registerClient(client:Client): Promise<Client>;

    retrieveAllClients(): Promise<Client[]>;

    retrieveClientByID(client_id:number): Promise<Client>;

    modifyClient(client: Client): Promise<Client>;

    removeClientByID(client_id:number): Promise<boolean>;

    registerAccount(account:Account): Promise<Account>;

    getAccountsByID(c_id:number): Promise<Account[]>;

    getAccounts(account_id:number):Promise<Account>

    updateAccount(account:Account):Promise<Account>;

    depositOrWithdrawIntoAccount(account:Account):Promise<Account>;

    removeAccountByID(account_id:number):Promise<boolean>;
}