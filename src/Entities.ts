
export class Client{
    constructor(
        public client_id:number,
        public first_name:string,
        public last_name:string
        )
        {}
}

export class Account{
    constructor(
        public account_id: number,
        public c_id:number,
        public balance:number
    )
    {}
}