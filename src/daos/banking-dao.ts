
import { Client } from "../Entities";
import { Account } from "../Entities";

export interface BankingDAO
{
    ///////////////// CLIENT

    //create 
    createClient(client:Client): Promise<Client>;
    // //read
    getAllClients(): Promise<Client[]>;
    getClientByID(client_id:number): Promise<Client>;
    // //update
    updateClient(client:Client): Promise<Client>;
    // //delete
    deleteClientByID(client_id:number): Promise<boolean>;

    // ///////////////// ACCOUNT

    // create
    createAccount(account:Account): Promise<Account>;
    // // read
    getAccountByID(c_id:number): Promise<Account[]>;
    getAccount(account_id:number): Promise<Account>
    // // update
    updateAccount(account:Account) :Promise<Account>;
    //
    updateAccountDepositOrWithdraw(account:Account): Promise<Account>
    // // delete
    deleteAccountByID(account_id:number): Promise<boolean>;

}