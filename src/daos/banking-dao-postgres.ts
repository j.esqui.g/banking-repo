import { Client } from "../Entities";
import { Account } from "../Entities";
import { BankingDAO } from "./banking-dao";
import { connection } from '../connection';
import { MissingResourceError } from "../errors";

export class BankingDaoPostgres implements BankingDAO{
 
    async createClient(client: Client): Promise<Client>
    {
        const sql:string = "insert into client(first_name,last_name) values ($1,$2) returning client_id"
        const values = [client.first_name,client.last_name];
        const result = await connection.query(sql,values);
        client.client_id = result.rows[0].client_id; // will get the latest id of the newest record that we are creating;
        return client;
    }

    async getAllClients(): Promise<Client[]>
    {
        const sql:string = "select * from client";
        const result = await connection.query(sql);
        const clients:Client[] = [];
        for(const row of result.rows)
        {
            const client:Client = new Client(row.client_id,row.first_name,row.last_name);
            clients.push(client);
        }
        return clients;
    }

    async getClientByID(client_id:number): Promise<Client>
    {
        const sql:string = 'select * from client where client_id = $1';
        const values = [client_id]
        const result = await connection.query(sql,values);
        const row = result.rows[0];
        if(result.rowCount=== 0)
        {
            throw new MissingResourceError(`The Client with id ${client_id} does not exist`);
        }
         const client:Client = new Client(row.client_id,row.first_name,row.last_name);
         return client;
    }

    async updateClient(client:Client): Promise<Client>
    {
        const sql:string = 'update client set first_name=$1, last_name=$2 where client_id=$3';
        const values = [client.first_name,client.last_name,client.client_id];
        const result = await connection.query(sql,values)
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The Client with id ${client.client_id} does not exist`);
        }
        return client;

    }

    async deleteClientByID(client_id:number): Promise<boolean>
    {
        const sql: string = 'delete from client where client_id =$1'
        const values = [client_id];
        const result = await connection.query(sql,values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The Client with id ${client_id} does not exist`);
        }
        return true;
    }

    async createAccount(account:Account): Promise<Account>
    {

        const sql:string = "insert into account(balance,c_id) values ($1,$2) returning account_id"
        const values = [account.balance,account.c_id];
        const result = await connection.query(sql,values);
        account.account_id = result.rows[0].account_id; // will get the latest id of the newest record that we are creating;
        return account;

    }

    // client could exist in client database, but maybe client doesnt have an account
    async getAccountByID(c_id: number): Promise<Account[]>
    {
        const sql:string = 'select * from account where c_id = $1'
        const values = [c_id];
        const result = await connection.query(sql,values);
        let accounts:Account[] = [];
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`No Account for Client with  id of ${c_id}`);
        }
       for(const row of result.rows)
       {
           const account:Account = new Account(row.account_id, row.c_id, row.balance);
           accounts.push(account);
       }

        return accounts;
    }

    
    async getAccount(account_id: number): Promise<Account>
    {
        const sql:string = 'select * from account where account_id = $1'
        const values = [account_id] = [account_id];
        const result = await connection.query(sql,values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`No Account with  id of ${account_id}`);
        }
        const account: Account = new Account(result.rows[0].account_id, result.rows[0].c_id, result.rows[0].balance)
        return account;
        
    }

    async updateAccount(account: Account): Promise<Account>
    {
        const sql:string = 'update account set balance = $1, c_id =$2 where account_id = $3';
        const values = [account.balance,account.c_id,account.account_id];
        const result = await connection.query(sql,values);

        return account;
        
    }

async deleteAccountByID(account_id:number): Promise<boolean>
{
    const sql: string = 'delete from account where account_id =$1'
    const values = [account_id];
    const result = await connection.query(sql,values);
    if(result.rowCount === 0)
    {
        throw new MissingResourceError(`The Account with id ${account_id} does not exist`);
    }
    return true;
}

async updateAccountDepositOrWithdraw(account:Account): Promise<Account>
{
    const sql:string = 'update account set balance = $1 where account_id = $2'
    const values = [account.balance, account.account_id];
    const result = await connection.query(sql,values);

    return account;
   
}

}

