import { Client } from "pg";

export const connection = new Client({
    user:'postgres',
    password: process.env.DBPASSWORD,
    database:'bankingdb',
    port:5432,
    host: '35.236.221.107'
})

connection.connect()